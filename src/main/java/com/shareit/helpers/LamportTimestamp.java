package com.shareit.helpers;

public class LamportTimestamp {
    private static LamportTimestamp lamportTimestamp;
    private static int localCounter;

    private LamportTimestamp(){}

    public static LamportTimestamp getLamportTimestampInstance(){
        if(lamportTimestamp == null) {
            synchronized (LamportTimestamp.class){
                if(lamportTimestamp == null) {
                    lamportTimestamp = new LamportTimestamp();
                }
            }

        }
        return lamportTimestamp;
    }

    public static int setLamportTimestamp(int messageLamportTimestamp) {
        localCounter = Math.max(localCounter, messageLamportTimestamp) + 1;
        return localCounter;
    }

    public static int getCurrentLamportTimestamp() {
        return ++localCounter;
    }
}
