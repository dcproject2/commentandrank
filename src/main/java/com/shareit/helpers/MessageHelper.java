package com.shareit.helpers;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.List;

public class MessageHelper {
    private static final int TIMESTAMP_INDEX = 7;
    private static final int IP_INDEX = 2;

    public String getLocalIP() {
        try {
            return InetAddress.getLocalHost().getHostAddress();
        } catch (UnknownHostException ex) {
            ex.printStackTrace();
        }
        return "0.0.0.0";
    }

    public long getCurrentTimeStamp() {
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        return timestamp.getTime();
    }

    public String getIPFromSearchQuery(List<String> query) {
        return query.get(IP_INDEX);

    }

    public String getTimestampFromSearchQuery(List<String> query) {
        return query.get(TIMESTAMP_INDEX);
    }

    /**
     * This method will construct a list by splitting the search message from spaces.
     */
    public List<String> getSearchMessageTokens(String searchMessage) {
        return Arrays.asList(searchMessage.split("\\s+"));
    }

    public String getQueryWithoutHopCount(String searchQuery) {
        return searchQuery.substring(0, searchQuery.lastIndexOf(Constants.SPACE));
    }
}
