package com.shareit.helpers;

import com.shareit.controllers.MessageController;
import com.shareit.objects.Comment;
import com.shareit.objects.Post;
import com.shareit.storages.PostStorage;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PostHelper {
    private MessageHelper messageHelper = new MessageHelper();
    private PostStorage postStorage = PostStorage.getPostStorage();
    private LamportTimestamp lamportTimestamp = LamportTimestamp.getLamportTimestampInstance();

    public String generateInitialPostCommand(String postMessage, String timeStamp) {
        String postID = constructPostID(timeStamp);
        Post post = new Post();
        post.setPostTimeStamp(timeStamp);
        post.setPostMessage(postMessage);
        post.setPostID(postID);
        post.setOwnerIP(messageHelper.getLocalIP());
        post.setOwnerPort(Integer.toString(MessageController.datagramSocket.getLocalPort()));
        post.setLamportTimeStamp(String.valueOf(lamportTimestamp.getCurrentLamportTimestamp()));
        postStorage.addPostToAllStorage(postID, post);
        postStorage.addPostToOwnerStorage(postID, post);

        String tempCmd = Constants.POST +
                Constants.SPACE + postID +
                Constants.SPACE + messageHelper.getLocalIP() +
                Constants.SPACE + MessageController.datagramSocket.getLocalPort() +
                Constants.SPACE + Constants.MAXIMUM_HOP_COUNT +
                Constants.SPACE + lamportTimestamp.getCurrentLamportTimestamp() +
                Constants.SPACE + 0.0 +
                Constants.COMMA_SEPARATOR + postMessage;
        return String.format("%4d", tempCmd.length() + Constants.LENGTH_CONSTANT) + Constants.SPACE + tempCmd;
    }

    public List<Comment> extractComments(List postMetadata, String postMessage) {
        List<Comment> comments = new ArrayList<>();
        if (postMessage.contains(Constants.OPEN_ANGULAR_BRACKET)) {
            int commentStartIndex = postMessage.indexOf(Constants.OPEN_ANGULAR_BRACKET);
            String commentMessage = postMessage.substring(commentStartIndex + 1, postMessage.length());
            String[] commentDetails = commentMessage.split(Constants.OPEN_ANGULAR_BRACKET);
            for (String commentDetail : commentDetails) {
                Comment comment = new Comment();
                List<String> commentSplit = Arrays.asList(commentDetail.split(Constants.COMMA_SEPARATOR));
                comment.setComment(commentSplit.get(0));
                comment.setCommentID(commentSplit.get(1));
                comment.setLamportTimeStamp(commentSplit.get(2).replace(Constants.CLOSE_ANGULAR_BRACKET, ""));
                comment.setAssociatedPostID(postMetadata.get(3).toString());
                comment.setCommentOwnerIP(postMetadata.get(4).toString());
                comment.setCommentOwnerPort(postMetadata.get(5).toString());
                comments.add(comment);
            }
        }
        return comments;

    }

    public String generatePostCommandForPropagation(List<String> postMetadata, String postMessage) {
        String tempCmd = postMetadata.get(2) +
                Constants.SPACE + postMetadata.get(3) +
                Constants.SPACE + postMetadata.get(4) +
                Constants.SPACE + postMetadata.get(5) +
                Constants.SPACE + (Integer.parseInt(postMetadata.get(6)) - 1) +
                Constants.SPACE + lamportTimestamp.setLamportTimestamp(Integer.parseInt(postMetadata.get(7))) +
                Constants.SPACE +
                Constants.SPACE + postMetadata.get(8) +
                Constants.COMMA_SEPARATOR + postMessage;
        return String.format("%4d", tempCmd.length() + Constants.LENGTH_CONSTANT) + Constants.SPACE + tempCmd;
    }

    private String constructPostID(String timeStamp) {
        return messageHelper.getLocalIP() + Constants.COLON + timeStamp;
    }

    public String generateInitialPostRankMessage(String postID, String timeStamp, String rank) {
        String tempCmd = Constants.RANK_POST +
                Constants.SPACE + postID +
                Constants.SPACE + constructPostID(timeStamp) +
                Constants.SPACE + messageHelper.getLocalIP() +
                Constants.SPACE + MessageController.datagramSocket.getLocalPort() +
                Constants.SPACE + lamportTimestamp.getCurrentLamportTimestamp() +
                Constants.SPACE + rank;
        return String.format("%4d", tempCmd.length() + Constants.LENGTH_CONSTANT) + Constants.SPACE + tempCmd;
    }
}
