package com.shareit.helpers;

import com.shareit.controllers.MessageController;

public class FlileRankHelper {
    MessageHelper messageHelper = new MessageHelper();

    public String generateFileRankMessage(String fileName,String fileOwnerIp,String fileOwnerPort, String rank) {
        String tempCmd = Constants.FILE_RANK +
                Constants.SPACE + rank +
                Constants.SPACE + constructFileRankID() +
                Constants.SPACE + messageHelper.getLocalIP() +
                Constants.SPACE + MessageController.datagramSocket.getLocalPort() +
                Constants.SPACE + LamportTimestamp.getCurrentLamportTimestamp() +
                Constants.SPACE +
                Constants.COMMA_SEPARATOR + fileName;
        return String.format("%4d", tempCmd.length() + Constants.LENGTH_CONSTANT) + Constants.SPACE + tempCmd;
    }

    private String constructFileRankID() {
        return messageHelper.getLocalIP() + Constants.COLON + messageHelper.getCurrentTimeStamp();
    }
}
