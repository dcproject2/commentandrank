package com.shareit.helpers;

import com.shareit.controllers.MessageController;
import com.shareit.objects.Comment;
import com.shareit.objects.Post;
import com.shareit.storages.PostStorage;
import com.shareit.views.PostViewer;

public class CommentHelper {
    private MessageHelper messageHelper = new MessageHelper();
    private LamportTimestamp lamportTimestamp = LamportTimestamp.getLamportTimestampInstance();
    private static PostStorage postStorage = PostStorage.getPostStorage();
    private PostViewer postViewer = new PostViewer();

    public String generateCommentMessage(String postID, String commentMessage) {
        String commentID = constructCommentID();
        if(null == postStorage.getOwnerPosts().get(postID)) {
            String tempCmd = Constants.COMMENT +
                    Constants.SPACE + postID +
                    Constants.SPACE + commentID +
                    Constants.SPACE + messageHelper.getLocalIP() +
                    Constants.SPACE + MessageController.datagramSocket.getLocalPort() +
                    Constants.SPACE + lamportTimestamp.getCurrentLamportTimestamp() +
                    Constants.COMMA_SEPARATOR + commentMessage;
            return String.format("%4d", tempCmd.length() + Constants.LENGTH_CONSTANT) + Constants.SPACE + tempCmd;
        } else {
            Comment comment = new Comment();
            comment.setAssociatedPostID(postID);
            comment.setCommentID(commentID);
            comment.setCommentOwnerIP(messageHelper.getLocalIP());
            comment.setCommentOwnerPort(Integer.toString(MessageController.datagramSocket.getLocalPort()));
            comment.setComment(commentMessage);
            comment.setLamportTimeStamp(String.valueOf(lamportTimestamp.getCurrentLamportTimestamp()));

            Post post = postStorage.getPostByID(postID);
            post.addComment(comment);

            postViewer.displayPostByID(null, post);
        }
        return commentID;
    }

    public String generatePostWithCommentsForPropagation(Post post) {
        StringBuilder comments = new StringBuilder();
        post.getComments().forEach(comment -> {
            comments.append(Constants.OPEN_ANGULAR_BRACKET);
            comments.append(comment.getComment());
            comments.append(Constants.COMMA_SEPARATOR);
            comments.append(comment.getCommentID());
            comments.append(Constants.COMMA_SEPARATOR);
            comments.append(comment.getLamportTimeStamp());
            comments.append(Constants.CLOSE_ANGULAR_BRACKET);});
        String tempCmd = Constants.POST +
                Constants.SPACE + post.getPostID() +
                Constants.SPACE + post.getOwnerIP() +
                Constants.SPACE + post.getOwnerPort() +
                Constants.SPACE + Constants.MAXIMUM_HOP_COUNT +
                Constants.SPACE + lamportTimestamp.getCurrentLamportTimestamp() +
                Constants.SPACE + post.getAveragePostRank() +
                Constants.COMMA_SEPARATOR + post.getPostMessage() + comments;
        return String.format("%4d", tempCmd.length() + Constants.LENGTH_CONSTANT) + Constants.SPACE + tempCmd;
    }

    private String constructCommentID() {
        return messageHelper.getLocalIP() + Constants.COLON + messageHelper.getCurrentTimeStamp();
    }
}
