package com.shareit.helpers;

import com.shareit.client.IncomingMessage;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.Arrays;
import java.util.List;

public class GenericHelper {
    public List messageDivider(IncomingMessage message) {
        return Arrays.asList(message.getMessage().split(Constants.COMMA_SEPARATOR, 2));
    }

    public List messageSplitter(String message) {
        return Arrays.asList(message.split("\\s+"));
    }

    public void sendDatagramToIP(String receiverIP, String receiverPort, String message) {
        try {
            byte[] messageBytes = message.getBytes();
            DatagramSocket datagramSocket = new DatagramSocket();
            datagramSocket.send(new DatagramPacket(messageBytes, messageBytes.length, InetAddress.getByName(receiverIP), Integer.parseInt(receiverPort)));
            System.out.println("\n*****Sending message : "+message+ " to: " + receiverIP + ":" + receiverPort + "*****");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

