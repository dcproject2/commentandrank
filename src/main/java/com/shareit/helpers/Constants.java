package com.shareit.helpers;

/*
* This interface contains all the constants
*/
public interface Constants {
    /*String constants*/
    String BS_IP = "Enter Bootstrap Server IP: ";
    String BS_PORT = "Enter Bootstrap Server Port: ";
    String NODE_ID = "Enter Your Index Number: ";

    String AVAILABLE_OPERATIONS = "1: Register, 2 : Search Query, 3 : Un register, 4 : Re register";

    String REG = "REG";
    String UN_REG = "UNREG";
    String UN_REG_OK = "UNROK";
    String JOIN = "JOIN";
    String JOIN_OK = "JOINOK";
    String REG_OK = "REGOK";
    String SER = "SER";
    String SER_OK = "SEROK";
    String LEAVE = "LEAVE";
    String LEAVE_OK = "LEAVEOK";
    String POST = "POST";
    String COMMENT = "COMMENT";
    String RANK_POST = "RANK_POST";
    String COMMA_SEPARATOR = ",";
    String OPEN_ANGULAR_BRACKET = "<";
    String CLOSE_ANGULAR_BRACKET = ">";
    String POST_MESSAGE_SEPERATOR_END = ":-|";
    String TAB = "\t";
    String COLON = ":";
    String FILE_RANK = "FILE_RANK";

    /*Character constants*/
    char SPACE = ' ';

    /*Integer constants*/
    int LENGTH_CONSTANT = 5;
    int MAXIMUM_HOP_COUNT = 5;
    int BUFFER_SIZE = 1000;
    int INITIAL_LAMPORT_TIMESTAMP = 0;
}
