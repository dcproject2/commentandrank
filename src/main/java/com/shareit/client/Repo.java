/*
* This class acts as a central repository for local data
* */
package com.shareit.client;

import com.shareit.helpers.MessageHelper;

import java.sql.Timestamp;
import java.util.*;
import java.util.stream.Collectors;

public class Repo {
    private String nodeID;
    private String serverIP;
    private int serverPort;
    private ArrayList<SearchEntry> searchEntries = new ArrayList<>();
    private List<String> localFileList = new ArrayList<>();
    private static Repo repo;
    private ArrayList<String> receivedSearchQueries = new ArrayList<>();
    private List<String> receivedPostRequests = new ArrayList<>();
    MessageHelper messageHelper = new MessageHelper();

    private Repo(){}

    public static Repo getRepoInstance() {
        if(null == repo) {
            synchronized (Repo.class) {
                repo = new Repo();
            }
        }
        return repo;
    }

    public void createLocalFileList() {
        Random r = new Random();
        Map<String, String> files = new HashMap<>();
        while(files.size() < 5) {
             int random = r.nextInt((20 - 1) + 1);
             files.put(getFileNameByIndex(random), getFileNameByIndex(random));
        }
        localFileList = files.keySet().stream().collect(Collectors.toList());
    }

    public ArrayList<SearchEntry> getSearchEntries() {
        return searchEntries;
    }

    public int getBootStrapServerPort() {
        return serverPort;
    }

    public void setServerPort(int serverPort) {
        this.serverPort = serverPort;
    }

    public int getOperation() {
        return operation;
    }

    public void setOperation(int operation) {
        this.operation = operation;
    }

    private int operation;

    public String getNodeID() {
        return nodeID;
    }

    public void setNodeID(String nodeID) {
        this.nodeID = nodeID;
    }

    public String getServerIP() {
        return serverIP;
    }

    public void setServerIP(String serverIP) {
        this.serverIP = serverIP;
    }

    public void addSearchEntry(String ip, String query, long timeStamp) {
        SearchEntry searchEntry = new SearchEntry();
        searchEntry.setOriginatorIP(ip);
        searchEntry.setSearchQuery(query);
        searchEntry.setTimeStamp(timeStamp);
        searchEntries.add(searchEntry);
    }

    private String getFileNameByIndex(int index) {
        List<String> files = new ArrayList<>();
        files.add("Adventures of Tintin");
        files.add("Jack and Jill");
        files.add("Glee");
        files.add("The Vampire Diarie");
        files.add("King Arthur");
        files.add("Windows XP");
        files.add("Harry Potter");
        files.add("Kung Fu Panda");
        files.add("Lady Gaga");
        files.add("Twilight");
        files.add("Windows 8");
        files.add("Mission Impossible");
        files.add("Turn Up The Music");
        files.add("Super Mario");
        files.add("American Pickers");
        files.add("Microsoft Office 2010");
        files.add("Happy Feet");
        files.add("Modern Family");
        files.add("American Idol");
        files.add("Hacking for Dummies");
        return files.get(index);
    }

    public void displayLocalFileList() {
        System.out.println("*****Files in the node*****");
        localFileList.forEach(System.out::println);
    }

    public List<String> getLocalFileList() {
        return localFileList;
    }

    public void removeOlderSearchEntries() {
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        for (SearchEntry searchEntry: searchEntries) {
            if(timestamp.getTime() - searchEntry.getTimeStamp() > 10000) {
                searchEntries.remove(searchEntry);
            }

        }
    }

    public List<String> getReceivedSearchQueries() {
        return receivedSearchQueries;
    }

    public void addSearchEntry(String searchQuery) {
        receivedSearchQueries.add(messageHelper.getQueryWithoutHopCount(searchQuery));
    }

    public void addPostRequest(String postRequest) {
        receivedPostRequests.add(messageHelper.getQueryWithoutHopCount(postRequest));
    }
}
