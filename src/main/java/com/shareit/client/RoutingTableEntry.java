package com.shareit.client;

public class RoutingTableEntry {
    private String peerIP;
    private String peerPort;
    private boolean status;

    public RoutingTableEntry(String peerIP, String peerPort, boolean status) {
        this.peerIP = peerIP;
        this.peerPort = peerPort;
        this.status = status;
    }

    public String getPeerIP() {
        return peerIP;
    }

    public void setPeerIP(String peerIP) {
        this.peerIP = peerIP;
    }

    public String getPeerPort() {
        return peerPort;
    }

    public void setPeerPort(String peerPort) {
        this.peerPort = peerPort;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}
