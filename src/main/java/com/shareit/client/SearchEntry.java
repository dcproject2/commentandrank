package com.shareit.client;

public class SearchEntry {
    String originatorIP;
    String searchQuery;
    Long timeStamp;

    public String getOriginatorIP() {
        return originatorIP;
    }

    public void setOriginatorIP(String originatorIP) {
        this.originatorIP = originatorIP;
    }

    public String getSearchQuery() {
        return searchQuery;
    }

    public void setSearchQuery(String searchQuery) {
        this.searchQuery = searchQuery;
    }

    public Long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Long timeStamp) {
        this.timeStamp = timeStamp;
    }
}
