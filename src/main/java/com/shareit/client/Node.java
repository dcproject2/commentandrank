/*
* This is the node class.
* */
package com.shareit.client;

import com.shareit.controllers.CommentController;
import com.shareit.controllers.MessageController;
import com.shareit.controllers.PostController;
import com.shareit.controllers.SearchController;
import com.shareit.controllers.FIleRankController;
import com.shareit.helpers.Constants;
import com.shareit.storages.RoutingTable;
import com.shareit.views.PostViewer;

import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Node {
    private static final int PING_TIME_DELAY = 10000;
    private static Scanner scanner = new Scanner(System.in);
    private static PostController postController = new PostController();
    private static FIleRankController fileRankController = new FIleRankController();
    private static CommentController commentController = new CommentController();
    private static PostViewer postViewer = new PostViewer();

    public static void main(String[] args) {
        MessageController messageController = new MessageController();

        //Get information from the user
        System.out.println(Constants.NODE_ID);
        Repo.getRepoInstance().setNodeID(scanner.nextLine());
        System.out.println(Constants.BS_IP);
        Repo.getRepoInstance().setServerIP(scanner.nextLine());
        System.out.println(Constants.BS_PORT);
        Repo.getRepoInstance().setServerPort(scanner.nextInt());
        Repo.getRepoInstance().createLocalFileList();
        Repo.getRepoInstance().displayLocalFileList();
        //Send registration request to the Bootstrap server
        messageController.registerInBootstrapServer(Repo.getRepoInstance());

        // scheduler to verify if the nodes are connected
        ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
        Runnable periodicTask = new Runnable() {
            public void run() {
                messageController.checkIfPeersAreReachable();
            }};
        executor.scheduleAtFixedRate(periodicTask, 0, PING_TIME_DELAY, TimeUnit.MILLISECONDS);
        nextOperation();

    }

    private static void nextOperation() {
        System.out.println("\n\nEnter an operation [unreg, query, files, peers, post, comment, rank-post, order, rank-file] :");
        //Listen for user queries
        while (true) {
            String operation = scanner.nextLine();
            switch (operation) {
                case "":
                    break;
                case "unreg":
                    unRegister();
                    break;
                case "query":
                    query();
                    break;
                case "files":
                    Repo.getRepoInstance().displayLocalFileList();
                    break;
                case "peers":
                    printPeers();
                    break;
                case "post":
                    initiatePost();
                    break;
                case "comment":
                    comment();
                    break;
                case "rank-post":
                    rankPost();
                    break;
                case "rank-file":
                    rankFile();
                    break;
                case "order":
                    postViewer.orderPost();
                    break;
                default:
                    System.out.println("Invalid operation.\nAccepted operations are: [unreg, query, files, peers, post, comment, rank-post, order, rank-file]\nEnter an operation again: ");
            }
        }
    }


    private static void unRegister() {
        MessageController messageController = new MessageController();
        messageController.unRegister(Repo.getRepoInstance());
    }


    private static void query() {
        SearchController searchController = new SearchController();
        System.out.println("Enter file name to search: ");
        searchController.searchInLocalRepository(scanner.nextLine());
    }

    private static void printPeers() {
        RoutingTable routingTable = new RoutingTable();
        Map<String, RoutingTableEntry> routingTableMap = routingTable.getRoutingTable();
        if(!routingTableMap.isEmpty()) {
            System.out.println("*****Printing neighbour peers*****");
            for (String key : routingTableMap.keySet()) {
                RoutingTableEntry entry = routingTableMap.get(key);
                System.out.println(entry.getPeerIP() + ":" + entry.getPeerPort());
            }
        } else {
            System.out.println("*****No neighbour peers*****");
        }
    }

    private static void initiatePost() {
        System.out.println("Enter your post:");
        postController.initiatePost(scanner.nextLine());
    }

    private static void comment(){
        String postID, comment;
        System.out.println("Enter post ID:");
        postID = scanner.nextLine();
        System.out.println("Enter a comment:");
        comment = scanner.nextLine();
        commentController.addComment(postID.trim(), comment);
    }

    private static void rankPost() {
        String postID;
        float rank;
        System.out.println("Enter post ID:");
        postID = scanner.nextLine();
        System.out.println("Enter a rank [1-5]:");
        rank = scanner.nextFloat();
        if(rank >= 1 && rank <= 5) {
            postController.rankPost(postID.trim(), Float.toString(rank));
        } else {
           System.out.println("Rank should be an integer between 1 and 5");
        }
    }

    private static void rankFile() {
        String filename;
        String fileOwnerIp;
        String fileOwnerPort;

        float rank;
        System.out.println("Enter FileName:");
        filename = scanner.nextLine();
        System.out.println("Enter Fileowner Ip:");
        fileOwnerIp = scanner.nextLine();
        System.out.println("Enter Fileowner port:");
        fileOwnerPort = scanner.nextLine();
        System.out.println("Enter a rank [1-5]:");
        rank = scanner.nextFloat();
        if(rank >= 1 && rank <= 5) {
            fileRankController.rankFile(filename.trim(),fileOwnerIp.trim(),fileOwnerPort.trim(), Float.toString(rank));
        } else {
            System.out.println("Rank should be an integer between 1 and 5");
        }
    }
}
