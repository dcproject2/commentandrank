package com.shareit.controllers;

import com.shareit.helpers.*;
import com.shareit.objects.Post;
import com.shareit.client.IncomingMessage;
import com.shareit.client.RoutingTableEntry;
import com.shareit.objects.PostRank;
import com.shareit.storages.PostStorage;
import com.shareit.storages.RoutingTable;
import com.shareit.views.PostViewer;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class PostController {

    private static MessageHelper messageHelper = new MessageHelper();
    private static PostStorage postStorage = PostStorage.getPostStorage();
    private PostHelper postHelper = new PostHelper();
    private PostViewer postViewer = new PostViewer();
    private GenericHelper genericHelper = new GenericHelper();
    private CommentHelper commentHelper = new CommentHelper();
    private CommentController commentController = new CommentController();

    public void initiatePost(String postMessage) {
        String timeStamp = Long.toString(messageHelper.getCurrentTimeStamp());
        String postRequest = postHelper.generateInitialPostCommand(postMessage, timeStamp);
        String postID = messageHelper.getLocalIP() + Constants.COLON + timeStamp;
        //TODO Need to create add comments to the post object
        //Repo.getRepoInstance().addPostRequest(postRequest);
        postViewer.displayPostByID(postID, null);
        sendInitialPostToNeighbours(postRequest);
    }

    //This method will view and store posts coming from other peers
    void incomingPostHandler(IncomingMessage message) {
        //Divide post message in to two by comma
        List<String> dividedPost = genericHelper.messageDivider(message);
        List<String> postMetadata = genericHelper.messageSplitter(dividedPost.get(0).toString());
        String postMessageWithComments = dividedPost.get(1).toString();
        String postMessage = Arrays.asList(dividedPost.get(1).split(Constants.OPEN_ANGULAR_BRACKET)).get(0);

        if(checkDuplicatePost(postMetadata, postMessageWithComments)) {
            //Store received post in the local storage
            storeOthersPosts(postMetadata, postMessage, postMessageWithComments);
            //Display received post
            postViewer.displayPostByID(postMetadata.get(3), null);
            int hopCount = Integer.parseInt(postMetadata.get(6));
            if (hopCount > 0) {
                sendInitialPostToNeighbours(postHelper.generatePostCommandForPropagation(postMetadata, postMessageWithComments));
            }
        } else {
            System.out.println("*****Ignored sending duplicate post to neighbours*****");
        }

    }

    public void sendInitialPostToNeighbours(String postMessage) {
        Map routingTable = RoutingTable.getRoutingTable();

        if(!routingTable.isEmpty()) {
            for (String peerIP : RoutingTable.getPeerIPAddresses()) {
                RoutingTableEntry routingTableEntry = (RoutingTableEntry) routingTable.get(peerIP);
                byte[] message = postMessage.getBytes();
                try {
                    //Open random socket to send search queries to neighbours
                    DatagramSocket datagramSocket = new DatagramSocket();
                    //Send search query to neighbour peers
                    datagramSocket.send(new DatagramPacket(message, message.length, InetAddress.getByName(routingTableEntry.getPeerIP()), Integer.parseInt(routingTableEntry.getPeerPort())));
                    System.out.println("\n*****Sending post to neighbours : "+postMessage+ " to: " + routingTableEntry.getPeerIP() +":"+routingTableEntry.getPeerPort()+"*****");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }  else {
            System.out.println("*****Routing table is empty. Ignored the post operation*****");
        }
    }

    //This method will store posts of other peers in the node local storage
    private Post storeOthersPosts(List postMetadata, String postMessage, String postMessageWithcomments) {
        String postID = postMetadata.get(3).toString();
        Post post = new Post();
        post.setPostID(postID);
        post.setOwnerIP(postMetadata.get(4).toString());
        post.setOwnerPort(postMetadata.get(5).toString());
        post.setPostMessage(postMessage);
        post.setHopCount("1");
        post.setLamportTimeStamp(postMetadata.get(7).toString());
        post.setAveragePostRank((Float.parseFloat(postMetadata.get(8).toString())));
        post.setComments(postHelper.extractComments(postMetadata, postMessageWithcomments));
        postStorage.addPostToAllStorage(postID, post);
        return post;
    }


    private boolean checkDuplicatePost(List<String> postMetadata, String postMessageWithComments) {
        String postID = postMetadata.get(3);
        Post post = postStorage.getPostByID(postID);
        float postAverageRank = Float.parseFloat(postMetadata.get(8));
        if(post != null && postHelper.extractComments(postMetadata, postMessageWithComments).size() == post.getComments().size()
                && Objects.equals(postHelper.extractComments(postMetadata, postMessageWithComments), post.getComments())
                && postAverageRank == post.getAveragePostRank()) {
            return false;
        }
        return true;
    }

    public void rankPost(String postID, String rank) {
        Post post  = postStorage.getPostByID(postID);
        String timeStamp = Long.toString(messageHelper.getCurrentTimeStamp());
        String rankMessage = postHelper.generateInitialPostRankMessage(postID, timeStamp, rank);
        //Check whether the post is owned by the node itself
        if(null == postStorage.getOwnerPostByID(postID)) {
            genericHelper.sendDatagramToIP(post.getOwnerIP(), post.getOwnerPort(), rankMessage);
        }
    }

    void incomingPostRankHandler(IncomingMessage incomingMessage) {
        List rankMessage = genericHelper.messageSplitter(incomingMessage.getMessage());
        String postID = rankMessage.get(3).toString();
        String rankID = rankMessage.get(4).toString();
        String rankerIP =  rankMessage.get(5).toString();
        //Create PostRank object
        PostRank postRank = new PostRank();
        postRank.setPostID(postID);
        postRank.setRankID(rankID);
        postRank.setRankerIP(rankerIP);
        postRank.setRankerPort(rankMessage.get(6).toString());
        postRank.setRank(Float.parseFloat(rankMessage.get(8).toString()));
        //Get rank record from history
        PostRank postRankRecord = postStorage.getPostRankByIP(rankerIP);
        //Eliminate duplicate post ranks. One node can rank one post only once
        if (null == postRankRecord || !postID.equals(postRankRecord.getPostID())) {
            //Add to post rank history
            postStorage.updatePostRankHistory(rankerIP, postRank);
            Post post = postStorage.getPostByID(postID);
            List<PostRank> postRanks = post.getPostRanks();
            postRanks.add(postRank);
            float totalPostRank = 0;
            //Calculate new post rank
            for (PostRank postrank : postRanks) {
                totalPostRank += postrank.getRank();
            }
            float averagePostRank = totalPostRank / postRanks.size();
            //Set new average post rank to the stored post object
            post.setAveragePostRank(averagePostRank);
            //Display post with new rank
            postViewer.displayPostByID(postID, null);
            //Send post with rank to neighbours
            String postMessageForPropagation = commentHelper.generatePostWithCommentsForPropagation(post);
            commentController.sendPostWithCommentToNeighbours(postMessageForPropagation);
        } else {
            System.out.println("*****Duplicate post rank received: " + incomingMessage.getMessage());
        }
    }
}
