/**
 * This class contains message control related codes
 */
package com.shareit.controllers;

import com.shareit.client.IncomingMessage;
import com.shareit.client.Repo;
import com.shareit.client.RoutingTableEntry;
import com.shareit.helpers.Constants;
import com.shareit.helpers.MessageHelper;
import com.shareit.storages.RoutingTable;
import com.shareit.views.PostViewer;

import java.io.IOException;
import java.net.*;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class MessageController {
    public static DatagramSocket datagramSocket;
    private static String localIP;
    private static MessageHelper messageHelper = new MessageHelper();
    private PostController postController = new PostController();
    private CommentController commentController = new CommentController();
    private FIleRankController fileRankController = new FIleRankController();

    public MessageController() {
        try {
            if(null == datagramSocket) {
                datagramSocket = new DatagramSocket();
            }
            localIP = InetAddress.getLocalHost().getHostAddress();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * This method will send registration request to the Bootstrap server
     */
    public void registerInBootstrapServer(Repo repo) {
        byte[] data = generateRegCommand(repo.getNodeID()).getBytes();
        sendRequest(data, repo);
    }

    /**
     * This method will send un registration request to the Bootstrap server
     */
    public void unRegister(Repo repo) {
        this.send(generateUnRegCommand(repo.getNodeID()),repo.getServerIP(),repo.getBootStrapServerPort());
    }

    private String generateRegCommand(String nodeID) {
        String tempCmd = Constants.REG + Constants.SPACE + messageHelper.getLocalIP() + Constants.SPACE + datagramSocket.getLocalPort() + Constants.SPACE + nodeID;
        return String.format("%4d", tempCmd.length() + Constants.LENGTH_CONSTANT) + Constants.SPACE + tempCmd;
    }

    private String generateUnRegCommand(String nodeID) {
        String tempCmd = Constants.UN_REG + Constants.SPACE + messageHelper.getLocalIP() + Constants.SPACE + datagramSocket.getLocalPort() + Constants.SPACE + nodeID;
        return String.format("%4d", tempCmd.length() + Constants.LENGTH_CONSTANT) + Constants.SPACE + tempCmd;
    }


    private void processResponse(List<String> response) {
        System.out.println("\n");
        switch (Integer.parseInt(response.get(2))) {
            case 9999:
                System.out.println("Failed, there is some error in the command");
//                this.closeSocket();
                break;
            case 9998:
                System.out.println("Failed, already registered to you, unregister first");
//                this.closeSocket();
                break;
            case 9997:
                System.out.println("Failed, registered to another user, try a different IP and port");
//                this.closeSocket();
                break;
            case 9996:
                System.out.println("Failed, cannot register. BS full");
//                this.closeSocket();
                break;
            case 0:
                System.out.println("Request is successful, no nodes in the system");
//                this.closeSocket();
            default:
                System.out.println("Request is successful, " + response.get(2) + " nodes in the system");
                startReceiving();
                joinWithPeers(response);

        }
    }


    private void joinWithPeers(List<String> response) {
        if (response.get(1).equals(Constants.REG_OK)) {
            int nodes = Integer.parseInt(response.get(2));

            if (nodes != 0) {
                for (int i = 0; i < nodes; i++) {
                    String joinReq = generateJoinCommand();
                    int port = Integer.parseInt(response.get(4+(i*2)));
                    send(joinReq, response.get(3+(i*2)), port);
                }
            }
        }
//        this.closeSocket();

    }

    private void startReceiving() {
        new Thread() {
            public void run() {
                while (datagramSocket != null && !datagramSocket.isClosed()) {
                    byte[] buffer = new byte[Constants.BUFFER_SIZE];
                    DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
                    try {
                        datagramSocket.receive(packet);
                        byte[] data = packet.getData();
                        String message = new String(data, 0, packet.getLength());
                        System.out.println("New Message Received" + message);
                      
                        IncomingMessage incomingMesg = new IncomingMessage();
                        incomingMesg.setMessage(message);
                        incomingMesg.setPort(packet.getPort()+"");
                        incomingMesg.setSenderIp(packet.getAddress().toString());

                        processRequest(incomingMesg);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }.start();
    }

    public void checkIfPeersAreReachable() {
        Map routingTable = RoutingTable.getRoutingTable();
        if (!routingTable.isEmpty()) {
            for (String peerIP : RoutingTable.getPeerIPAddresses()) {
                RoutingTableEntry routingTableEntry = (RoutingTableEntry) routingTable.get(peerIP);
                boolean reachable = false;
                try {
                    reachable = InetAddress.getByName(routingTableEntry.getPeerIP()).isReachable(60);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (!reachable) {
                    System.out.println("Node " + peerIP +":"+routingTableEntry.getPeerPort()+" is not reachable");
                    if(RoutingTable.routingTable.containsKey(peerIP)){
                        RoutingTable.routingTable.remove(peerIP);
                        System.out.println(peerIP + " ip is removed from the routing table");
                    }
                }
            }
        }
    }

    private void processRequest(IncomingMessage message) {
        SearchController searchController = new SearchController();
        List<String> response = Arrays.asList(message.getMessage().split("\\s+"));

        switch (response.get(2)) {
            case Constants.JOIN:
                String joinRes = generateJoinResponse("0");
                RoutingTable.updateRoutingTableEntry(response.get(3), response.get(4), true);
                int port = Integer.parseInt(response.get(4));
                send(joinRes, response.get(3), port);
                break;
            case Constants.JOIN_OK:
                updateRoutingTable(response.get(3),response.get(4), response.get(5));
                break;
            case Constants.LEAVE:
                RoutingTable.removeEntryFromRoutingTable(response.get(3));
                break;
            case Constants.SER:
                searchController.searchResult(message.getMessage(), convertMessageInToList(message.getMessage()).get(3), convertMessageInToList(message.getMessage()).get(4));
                break;
            case Constants.SER_OK:
                displaySearchResult(response.get(4), response.get(5));
                break;
            case Constants.POST:
                postController.incomingPostHandler(message);
                break;
            case Constants.COMMENT:
                commentController.incomingCommentHandler(message);
                break;
            case Constants.RANK_POST:
                postController.incomingPostRankHandler(message);
                break;
            case Constants.FILE_RANK:
                fileRankController.incomingFileRankHandler(message);
                break;
            default:
                System.out.println("Wrong message!!!");

        }
        if(Constants.UN_REG_OK.equals(response.get(1))) {
            sendLeaveRequestToPeers();
        }
    }

    private String generateJoinCommand() {
        String tempCmd = Constants.JOIN + Constants.SPACE + localIP + Constants.SPACE + datagramSocket.getLocalPort();
        return String.format("%4d", tempCmd.length() + Constants.LENGTH_CONSTANT) + Constants.SPACE + tempCmd;
    }

    private String generateJoinResponse(String result) {
        String tempCmd = Constants.JOIN_OK + Constants.SPACE + result + Constants.SPACE + localIP + Constants.SPACE + datagramSocket.getLocalPort();
        return String.format("%4d", tempCmd.length() + Constants.LENGTH_CONSTANT) + Constants.SPACE + tempCmd;
    }

    private void send(String req, String ip, int port) {

        DatagramPacket packet;
        try {
            packet = new DatagramPacket(req.getBytes(), req.getBytes().length, InetAddress.getByName(ip), port);
            datagramSocket.send(packet);
            //TODO get response from the peer
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void sendRequest(byte[] data, Repo repo) {
        try {
            byte[] response = new byte[65536];

            datagramSocket.send(new DatagramPacket(data, data.length, InetAddress.getByName(repo.getServerIP()), repo.getBootStrapServerPort()));

            DatagramPacket packet = new DatagramPacket(response, response.length);
            datagramSocket.receive(packet);
            System.out.println("\nNode IP: "+messageHelper.getLocalIP()+":"+datagramSocket.getLocalPort());
            String serverResponse = new String(packet.getData(), 0, packet.getLength());

            List<String> result = Arrays.asList(serverResponse.split("\\s+"));
            if(!Constants.UN_REG_OK.equals(result.get(1))) {
                processResponse(result);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void sendLeaveRequestToPeers() {
        Map routingTable = RoutingTable.getRoutingTable();
        String leaveMessage = generateLeaveRequest();
        if(!routingTable.isEmpty()) {
            byte[] leaveQuery = leaveMessage.getBytes();
            for (String peerIP : RoutingTable.getPeerIPAddresses()) {
                RoutingTableEntry routingTableEntry = (RoutingTableEntry) routingTable.get(peerIP);
                try {
                    DatagramSocket datagramSocket = new DatagramSocket();
                    //Send leave message to neighbour peers
                    System.out.println("\n*****Sending leave message to: " + routingTableEntry.getPeerIP() + "*****");
                    datagramSocket.send(new DatagramPacket(leaveQuery, leaveQuery.length, InetAddress.getByName(routingTableEntry.getPeerIP()), Integer.parseInt(routingTableEntry.getPeerPort())));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }  else {
            System.out.println("*****Routing table is empty. No leave message required*****");
        }
        closeSocket();
        System.out.println("*****Node Socket closed*****");
    }

    private String generateLeaveRequest() {
        String tempCmd = Constants.LEAVE + Constants.SPACE + messageHelper.getLocalIP() + Constants.SPACE + datagramSocket.getLocalPort();
        return String.format("%4d", tempCmd.length() + Constants.LENGTH_CONSTANT) + Constants.SPACE + tempCmd;
    }
    private String generateLeaveResponse(String result) {
        String tempCmd = Constants.LEAVE_OK + Constants.SPACE + 0;
        return String.format("%4d", tempCmd.length() + Constants.LENGTH_CONSTANT) + Constants.SPACE + tempCmd;
    }

    private void closeSocket() {
        this.datagramSocket.close();
    }

    /**
     * This method will initiate the search with peer nodes
     */
    public void sendQueryToNeighbourPeers(String queryMessage) {
        Map routingTable = RoutingTable.getRoutingTable();

        if(!routingTable.isEmpty()) {
            for (String peerIP : RoutingTable.getPeerIPAddresses()) {
                RoutingTableEntry routingTableEntry = (RoutingTableEntry) routingTable.get(peerIP);
                byte[] searchQuery = queryMessage.getBytes();
                try {
                    //Open random socket to send search queries to neighbours
                    DatagramSocket datagramSocket = new DatagramSocket();
                    //Send search query to neighbour peers
                    datagramSocket.send(new DatagramPacket(searchQuery, searchQuery.length, InetAddress.getByName(routingTableEntry.getPeerIP()), Integer.parseInt(routingTableEntry.getPeerPort())));
                    System.out.println("\n*****Sending search query : "+queryMessage+ " to: " + routingTableEntry.getPeerIP() +":"+routingTableEntry.getPeerPort()+"*****");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }  else {
            System.out.println("*****Routing table is empty. Ignored the search query*****");
        }
    }

    public void sendFileFoundMessageToRequester(String message, String requesterIP, String requesterPort) {
        try {
            byte[] response = message.getBytes();
            DatagramSocket datagramSocket = new DatagramSocket();
            System.out.println("*****Send file found message: "+ message + " to: " +requesterIP+ "*****");
            datagramSocket.send(new DatagramPacket(response, response.length, InetAddress.getByName(requesterIP), Integer.parseInt(requesterPort)));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<String> convertMessageInToList(String message) {
        return Arrays.asList(message.split("\\s+"));
    }

    public void updateRoutingTable(String response,String senderIP, String senderPort){

        if("0".equals(response)){
            RoutingTable.updateRoutingTableEntry(senderIP, senderPort, true);
        }else{
            RoutingTable.removeEntryFromRoutingTable(senderIP);
        }
    }

    private void displaySearchResult(String message, String port) {
         System.out.println("++++++ Your requested file was found in the node :"+ message +":"+port);
    }

}