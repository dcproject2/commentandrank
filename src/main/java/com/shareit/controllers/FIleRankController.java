package com.shareit.controllers;

import com.shareit.client.IncomingMessage;
import com.shareit.client.RoutingTableEntry;
import com.shareit.helpers.FlileRankHelper;
import com.shareit.helpers.GenericHelper;
import com.shareit.helpers.MessageHelper;
import com.shareit.objects.Comment;
import com.shareit.objects.FileRank;
import com.shareit.objects.Post;
import com.shareit.storages.FileRankStorage;
import com.shareit.storages.PostStorage;
import com.shareit.storages.RoutingTable;
import com.shareit.views.PostViewer;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class FIleRankController {
    private FlileRankHelper fileRankHelper = new FlileRankHelper();
    private FileRankStorage fileRankStorage = FileRankStorage.getFileRankStorage();
    private GenericHelper genericHelper = new GenericHelper();
    private PostViewer postViewer = new PostViewer();


    void incomingFileRankHandler(IncomingMessage message) {
        List dividedMessage = genericHelper.messageDivider(message);
        List commentInformation = genericHelper.messageSplitter(dividedMessage.get(0).toString());
        String fileName = dividedMessage.get(1).toString();
        String fileRankerIp = commentInformation.get(5).toString();
        FileRank fileRank = new FileRank();
        fileRank.setRank(Double.parseDouble(commentInformation.get(3).toString()));
        fileRank.setFileID(commentInformation.get(4).toString());
        fileRank.setRankerIP(fileRankerIp);
        fileRank.setRankerPort(commentInformation.get(6).toString());
        fileRank.setLamportTimeStamp(commentInformation.get(7).toString());
        fileRank.setFileName(fileName);

        if(!isDuplicateRankMsg(fileRank,fileName)) {

            double fileRankValue = 0.0;
            FileRank duplicateFileRank = fileRankStorage.getFileRankByFileNameAndID(fileName,  commentInformation.get(4).toString());

            if (duplicateFileRank == null) {
                fileRankStorage.addFileRankToStorage(fileName, commentInformation.get(4).toString(), fileRank);
                fileRankValue = fileRankStorage.rank(fileName);
                FileRankStorage.fileRanksMap.put(fileName,fileRankValue);
                System.out.println(" ############# File Ranking For File " + fileName + " is " + fileRankValue);
                Map<String, RoutingTableEntry> routingTable = RoutingTable.getRoutingTable();
                for (String peerIp : routingTable.keySet()) {
                    genericHelper.sendDatagramToIP(routingTable.get(peerIp).getPeerIP(), routingTable.get(peerIp).getPeerPort(), message.getMessage());
                }
            }
        }else{
            System.out.println(" ############# Got Duplicate file rank Msg " + message.getMessage());
        }

    }

    public void rankFile(String fileName,String fileOwnerIp,String fileOwnerPort, String rank) {

        String rankMessage = fileRankHelper.generateFileRankMessage(fileName,fileOwnerIp,fileOwnerPort, rank);
        genericHelper.sendDatagramToIP(fileOwnerIp,fileOwnerPort, rankMessage);

    }

    public boolean isDuplicateRankMsg(FileRank fileRank, String fileName) {
        String id = fileRank.getFileID();
        if (FileRankStorage.allFileRanks.containsKey(fileName)) {
            HashMap<String, FileRank> rankMap = ((HashMap<String, FileRank>) FileRankStorage.allFileRanks.get(fileName));
            if (rankMap.containsKey(id)) {
             return true;
            }
            return false;
        }
        return false;
    }
}
