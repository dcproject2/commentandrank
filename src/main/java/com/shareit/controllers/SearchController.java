package com.shareit.controllers;

import com.shareit.client.Repo;
import com.shareit.helpers.Constants;
import com.shareit.helpers.MessageHelper;
import com.shareit.storages.FileRankStorage;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SearchController {
    private static MessageHelper messageHelper = new MessageHelper();
    private static final int HOP_TOKEN_INDEX = 1;
    private static final int ZERO = 0;

    /**
     * This method will process search requests coming from other peers
     * @param searchCommand complete search query coming from the neighbour peer
     **/
    public void searchResult(String searchCommand, String senderIP, String senderPort) {
        //Check whether the search query is duplicate
        if (!matchSearchQueries(searchCommand)) {
            //Check the hop count
            int hopCount = getHopFromCommand(searchCommand);
            if (hopCount != ZERO) {
                String searchPhrase = extractSearchPhrase(searchCommand);
                //Long searchMessageOriginTimeStamp = Long.parseLong(messageHelper.getTimestampFromSearchQuery(messageHelper.getSearchMessageTokens(searchCommand)));
                //Update search entry list
                //Repo.getRepoInstance().addSearchEntry(messageHelper.getIPFromSearchQuery(messageHelper.getSearchMessageTokens(searchCommand)), searchPhrase, searchMessageOriginTimeStamp);
                Repo.getRepoInstance().addSearchEntry(searchCommand);
                MessageController messageController = new MessageController();
                String result;
                String[] commandSplit = searchCommand.split(" ");
                String hop = Integer.toString(getHopFromCommand(searchCommand));
                //Search the file in the local files list first
                List matchedFiles = searchFile(searchPhrase);

                if (matchedFiles.isEmpty()) {
                    System.out.println("*****File not found in the local storage trying to forward request to neighbour peers*****");
                    result = searchCommandForPropagation(searchCommand);
                    messageController.sendQueryToNeighbourPeers(result);
                } else {
                    List filesRanks= getFilesRankList(matchedFiles);
                    result = searchFileFoundCommand(matchedFiles, commandSplit[2], commandSplit[3],
                            searchPhrase, hop , filesRanks);
                    //Sending response to the file requester
                    messageController.sendFileFoundMessageToRequester(result, senderIP, senderPort);
                }
            } else {
                System.out.println("*****Ignored the search query with 0 hop count: " + searchCommand);
            }
        } else {
            System.out.println("*****Ignored the duplicate search query: " + searchCommand);
        }
    }

    /**
     * This method will search the file in local file list
     * If not found send the request to the neighbour peers
     */
    public void searchInLocalRepository(String searchPhrase) {
        System.out.println("*****Search file in the local files list*****");
        String searchCommand = constructInitialSearchCommand(searchPhrase);
        if(!matchSearchQueries(searchCommand)) {
            //Update search entries list
            Repo.getRepoInstance().addSearchEntry(searchCommand);
            List matchedFiles = searchFile(searchPhrase);
            MessageController messageController = new MessageController();
            if (matchedFiles.isEmpty()) {
                System.out.println("*****File not found in the local file list trying to forward request to neighbour peers*****");
//                String result = constructInitialSearchCommand(searchPhrase);
                messageController.sendQueryToNeighbourPeers(searchCommand);
            } else {
                System.out.println("+++++ File(s) found in the local file list " + matchedFiles);
            }
        } else {
            System.out.println("*****Ignored the duplicate search query*****");
        }
    }

    /**
     * Extract the search phrase from the command
     * @param command
     * @return
     */
    private String extractSearchPhrase(String command) {
        String searchPhrase = "";
        Pattern pattern = Pattern.compile("\"([^\"]*)\"");
        Matcher matcher = pattern.matcher(command);
        while (matcher.find()) {
            searchPhrase = matcher.group(1);
            System.out.println(matcher.group(1));
        }
        return searchPhrase;
    }




    /**
     * Search for matching files
     * @param searchPhrase
     * @return
     */
    private List<String> searchFile(String searchPhrase) {

        List<String> matchedFile = new ArrayList<>();
        List<String> tokens = Arrays.asList(searchPhrase.split(" "));
        StringBuilder patternString = new StringBuilder();
        patternString.append("^");
        for (String token : tokens) {
            patternString.append("(?=.*\\b");
            patternString.append(token);
            patternString.append("\\b)");
        }

        patternString.append(".+");
        Pattern pattern = Pattern.compile(patternString.toString(), Pattern.CASE_INSENSITIVE);

        for (String file : Repo.getRepoInstance().getLocalFileList()) {
            if (pattern.matcher(file).find()) {
                matchedFile.add(file);
            }
        }
        return matchedFile;
    }

    private String constructInitialSearchCommand(String searchPhrase) {
        MessageHelper messageHelper = new MessageHelper();
        messageHelper.getLocalIP();
        String tempCmd = "SER" + Constants.SPACE + messageHelper.getLocalIP() + Constants.SPACE + MessageController.datagramSocket.getLocalPort() + Constants.SPACE +
                "\"" + searchPhrase + "\"" +Constants.SPACE + Long.toString(messageHelper.getCurrentTimeStamp()) + Constants.SPACE + Constants.MAXIMUM_HOP_COUNT;

        return String.format("%4d", tempCmd.length() + Constants.LENGTH_CONSTANT) + Constants.SPACE + tempCmd;
    }
    private String searchCommandForPropagation (String searchReq) {
        int lastIndexOfSpace = searchReq.lastIndexOf(Constants.SPACE);
        String searchCommand = searchReq.substring(Constants.LENGTH_CONSTANT, lastIndexOfSpace);
        // reduce hop by one count before propagation
        int hopCount  = getHopFromCommand(searchReq) - 1;
        String tempCmd = searchCommand + Constants.SPACE + hopCount;
        return String.format("%4d", tempCmd.length() + Constants.LENGTH_CONSTANT) + Constants.SPACE + tempCmd;
    }
    private String searchFileFoundCommand(List<String> matchedFiles, String requestIP, String requestPort, String searchString, String hops,List<Double> fileRanks) {
        //length SEROK no_files IP port hops filename1, filename2
        String tempCmd = "SEROK" + Constants.SPACE + matchedFiles.size() + Constants.SPACE + messageHelper.getLocalIP() + Constants.SPACE + MessageController.datagramSocket.getLocalPort()
                + Constants.SPACE + hops + Constants.SPACE + matchedFiles + Constants.SPACE + fileRanks;
        return String.format("%4d", tempCmd.length() + Constants.LENGTH_CONSTANT) + Constants.SPACE + tempCmd;
    }

    private int getHopFromCommand(String command) {
        List<String> searchCommandTokens  = messageHelper.getSearchMessageTokens(command);
        return Integer.parseInt(searchCommandTokens.get(searchCommandTokens.size()-HOP_TOKEN_INDEX));
    }

    private boolean matchSearchQueries(String searchQuery) {
        List<String> searchQueries = Repo.getRepoInstance().getReceivedSearchQueries();
        String searchMessage = messageHelper.getQueryWithoutHopCount(searchQuery);
        return searchQueries.stream().anyMatch(entry -> entry.equals(searchMessage));
    }

    /**
    * This method can be used to find search queries.
    */
//    private boolean matchSearchEntries(ArrayList<SearchEntry> searchEntries, String ip, String query, long timestamp) {
//        return searchEntries.stream().anyMatch(entry -> ip.equals(entry.getOriginatorIP()) && query.equals(entry.getSearchQuery()) && entry.getTimeStamp() == timestamp);
//    }

    private List<Double> getFilesRankList(List<String> matchedFiles){
        List<Double> rankList= new LinkedList<>();
        for(String fileName : matchedFiles){
           if( FileRankStorage.fileRanksMap.containsKey(fileName)){
               Double rank =(Double) FileRankStorage.fileRanksMap.get(fileName);
               rankList.add(rank);
           }else{
               rankList.add(0.0);
           }
        }
        return  rankList;
    }
}
