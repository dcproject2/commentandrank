package com.shareit.objects;

public class Comment {
    private String commentID;
    private String comment;
    private String commentOwnerIP;
    private String commentOwnerPort;
    private String associatedPostID;
    private String lamportTimeStamp;

    public String getCommentID() {
        return commentID;
    }

    public void setCommentID(String commentID) {
        this.commentID = commentID;
    }

    public String getComment() {
        return this.comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getCommentOwnerIP() {
        return this.commentOwnerIP;
    }

    public void setCommentOwnerIP(String commentOwnerIP) {
        this.commentOwnerIP = commentOwnerIP;
    }

    public String getCommentOwnerPort() {
        return this.commentOwnerPort;
    }

    public void setCommentOwnerPort(String commentOwnerPort) {
        this.commentOwnerPort = commentOwnerPort;
    }

    public String getAssociatedPostID() {
        return this.associatedPostID;
    }

    public void setAssociatedPostID(String associatedPostID) {
        this.associatedPostID = associatedPostID;
    }

    public String getLamportTimeStamp() {
        return lamportTimeStamp;
    }

    public void setLamportTimeStamp(String lamportTimeStamp) {
        this.lamportTimeStamp = lamportTimeStamp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Comment)) return false;

        Comment comment1 = (Comment) o;

        if (getCommentID() != null ? !getCommentID().equals(comment1.getCommentID()) : comment1.getCommentID() != null)
            return false;
        return getAssociatedPostID() != null ? getAssociatedPostID().equals(comment1.getAssociatedPostID()) : comment1.getAssociatedPostID() == null;
    }

    @Override
    public int hashCode() {
        int result = getCommentID() != null ? getCommentID().hashCode() : 0;
        result = 31 * result + (getAssociatedPostID() != null ? getAssociatedPostID().hashCode() : 0);
        return result;
    }
}
