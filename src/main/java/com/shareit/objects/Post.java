package com.shareit.objects;

import java.util.ArrayList;
import java.util.List;

public class Post {
    private String postID;
    private String postTimeStamp;
    private String postMessage;
    private String ownerIP;
    private String ownerPort;
    private String hopCount;
    private String lamportTimeStamp;
    private List<Comment> comments = new ArrayList<>();
    private List<PostRank> postRanks = new ArrayList<>();
    private float averagePostRank;

    public void setPostRanks(List<PostRank> postRanks) {
        this.postRanks = postRanks;
    }

    public float getAveragePostRank() {
        return averagePostRank;
    }

    public void setAveragePostRank(float averagePostRank) {
        this.averagePostRank = averagePostRank;
    }

    public List<PostRank> getPostRanks() {
        return postRanks;
    }

    public String getHopCount() {
        return hopCount;
    }

    public void setHopCount(String hopCount) {
        this.hopCount = hopCount;
    }

    public String getLamportTimeStamp() {
        return lamportTimeStamp;
    }

    public void setLamportTimeStamp(String lamportTimeStamp) {
        this.lamportTimeStamp = lamportTimeStamp;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public String getPostID() {
        return postID;
    }

    public void setPostID(String postID) {
        this.postID = postID;
    }

    public String getPostTimeStamp() {
        return postTimeStamp;
    }

    public void setPostTimeStamp(String postTimeStamp) {
        this.postTimeStamp = postTimeStamp;
    }

    public String getPostMessage() {
        return postMessage;
    }

    public void setPostMessage(String postMessage) {
        this.postMessage = postMessage;
    }

    public String getOwnerIP() {
        return ownerIP;
    }

    public void setOwnerIP(String ownerIP) {
        this.ownerIP = ownerIP;
    }

    public String getOwnerPort() {
        return ownerPort;
    }

    public void setOwnerPort(String ownerPort) {
        this.ownerPort = ownerPort;
    }

    public void addComment(Comment comment) {
        comments.add(comment);
    }

    public void addRank(PostRank postRank) {
        postRanks.add(postRank);
    }
}
