package com.shareit.objects;

public class FileRank {

    private String FileID;
    private double rank;
    private String fileName;
    private String rankerIP;
    private String rankerPort;
    private String lamportTimeStamp;

    public String getFileID() {
        return FileID;
    }

    public void setFileID(String fileID) {
        FileID = fileID;
    }

    public double getRank() {
        return rank;
    }

    public void setRank(double rank) {
        this.rank = rank;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getRankerIP() {
        return rankerIP;
    }

    public void setRankerIP(String rankerIP) {
        this.rankerIP = rankerIP;
    }

    public String getRankerPort() {
        return rankerPort;
    }

    public void setRankerPort(String rankerPort) {
        this.rankerPort = rankerPort;
    }

    public String getLamportTimeStamp() {
        return lamportTimeStamp;
    }

    public void setLamportTimeStamp(String lamportTimeStamp) {
        this.lamportTimeStamp = lamportTimeStamp;
    }


    @Override
    public boolean equals(Object obj) {

        if (obj == this) {
            return true;
        }

        if (!(obj instanceof FileRank)) {
            return false;
        }

        FileRank c = (FileRank) obj;

       return c.fileName.equals(fileName) && c.rankerIP.equals(rankerIP) && c.lamportTimeStamp==lamportTimeStamp;

        }

        @Override
        public int hashCode() {
            int hash = 3;
            hash = 53 * hash + (this.rankerIP != null ? this.rankerIP.hashCode() : 0);
            hash = 53 * hash + (this.fileName != null ? this.fileName.hashCode() : 0);
            hash = 53 * hash + (this.lamportTimeStamp != null ? this.lamportTimeStamp.hashCode() : 0);
            return hash;
        }
}
