package com.shareit.objects;

public class PostRank {
    private String postRankOwner;
    private String postID;
    private String rankID;
    private float rank;
    private String rankerIP;
    private String rankerPort;

    public String getRankerIP() {
        return rankerIP;
    }

    public void setRankerIP(String rankerIP) {
        this.rankerIP = rankerIP;
    }

    public String getRankerPort() {
        return rankerPort;
    }

    public void setRankerPort(String rankerPort) {
        this.rankerPort = rankerPort;
    }

    public String getPostRankOwner() {
        return postRankOwner;
    }

    public void setPostRankOwner(String postRankOwner) {
        this.postRankOwner = postRankOwner;
    }

    public String getPostID() {
        return postID;
    }

    public void setPostID(String postID) {
        this.postID = postID;
    }

    public float getRank() {
        return rank;
    }

    public void setRank(float rank) {
        this.rank = rank;
    }

    public String getRankID() {
        return rankID;
    }

    public void setRankID(String rankID) {
        this.rankID = rankID;
    }
}
