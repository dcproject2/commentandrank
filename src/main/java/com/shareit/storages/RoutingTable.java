package com.shareit.storages;

import com.shareit.client.RoutingTableEntry;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public class RoutingTable {

    public static ConcurrentHashMap<String, RoutingTableEntry> routingTable = new ConcurrentHashMap<>();

    public static void updateRoutingTableEntry(String peerIP, String peerPort, boolean status) {
        routingTable.put(peerIP, new RoutingTableEntry(peerIP, peerPort, status));
    }

    public static Set<String> getPeerIPAddresses() {
        return routingTable.keySet();
    }

    public static void removeEntryFromRoutingTable(String senderIP){
        if(routingTable.containsKey(senderIP)){
            routingTable.remove(senderIP);
            System.out.println(senderIP+" ip is removed from the routing table");
        }
    }

    public static Map getRoutingTable() {
        return routingTable;
    }

}
