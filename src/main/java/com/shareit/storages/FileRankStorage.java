package com.shareit.storages;

import com.shareit.objects.FileRank;

import java.util.HashMap;

public class FileRankStorage {


    private static FileRankStorage fileRankStorage;

    private FileRankStorage(){}


    public static FileRankStorage getFileRankStorage() {
        if(fileRankStorage == null) {
            fileRankStorage = new FileRankStorage();
        }
        return fileRankStorage;
    }

    public static HashMap allFileRanks = new HashMap<String, HashMap<String,FileRank>>();
    public static HashMap fileRanksMap = new HashMap<String, Double>();

    public static HashMap getAllFileRank() {
        return allFileRanks;
    }

    public void addFileRankToStorage(String fileName, String id, FileRank fileRank) {
        if(allFileRanks.containsKey(fileName)){
            HashMap<String,FileRank> rankMap = ((HashMap<String,FileRank>)allFileRanks.get(fileName));
            if(rankMap.containsKey(id)){
                if(!rankMap.get(id).equals(fileRank)){
                    rankMap.put(id,fileRank);
                    allFileRanks.put(fileName,rankMap);
                }
            }else{
                rankMap.put(id,fileRank);
                allFileRanks.put(fileName,rankMap);
            }
        }else{
            HashMap rankIpMap = new HashMap<String,FileRank>();
            rankIpMap.put(id,fileRank);
            allFileRanks.put(fileName,rankIpMap);
        }
    }

    public FileRank getFileRankByFileNameAndID(String fileName, String fileRankerId) {
        if(allFileRanks.containsKey(fileName)){
            HashMap<String,FileRank> rankMap = ((HashMap<String,FileRank>)allFileRanks.get(fileName));
            if(rankMap.containsKey(fileRankerId)){
                return  rankMap.get(fileRankerId);
            }else{
                return null;
            }

        }else{
            return null;
        }
    }

    public double rank(String fileName) {
        double fileRank=0.0;
        if(allFileRanks.containsKey(fileName)){
            HashMap<String,FileRank> rankMap = ((HashMap<String,FileRank>)allFileRanks.get(fileName));
            for(String rankingFileName : rankMap.keySet()){
                fileRank += rankMap.get(rankingFileName).getRank();
            }
            fileRank = fileRank / rankMap.keySet().size();
            return fileRank;
        }else{
            return fileRank;
        }
    }

}
