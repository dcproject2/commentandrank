package com.shareit.views;

import com.shareit.helpers.Constants;
import com.shareit.objects.Comment;
import com.shareit.objects.Post;
import com.shareit.storages.PostStorage;

import java.text.DecimalFormat;
import java.util.Comparator;
import java.util.List;

public class PostViewer {
    PostStorage postStorage = PostStorage.getPostStorage();

//    public void displayPost(String postID, String post) {
//        System.out.println("|||||||||||||||||||||||||***POST***|||||||||||||||||||||||||");
//        System.out.println(Constants.TAB + postID + " :::: " + post);
//        System.out.println("\t\t>>>>>>>>>>Test comment for the post");
//        System.out.println("||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||");
//        System.out.println();
//    }

    public void displayPosts() {
        postStorage.getAllPosts().forEach((posID, post) -> {
            System.out.println("||||||||||||||||||||||||||||||||||||||||||||||||||***POST***||||||||||||||||||||||||||||||||||||||||||||||||||");
            System.out.println("POST >>" + Constants.TAB +
                    ((Post) post).getPostID() + " :::: " +
                    ((Post) post).getPostMessage());
            ((Post) post).getComments().forEach(comment ->
                    System.out.println("COMMENT >>" +
                            Constants.TAB + comment.getCommentID() +
                            " :::: " + comment.getComment()));
            System.out.println("||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||");
            System.out.println();
        });
    }

    public void orderPost() {
        if (!postStorage.getAllPosts().isEmpty()) {
            System.out.println("||||||||||||||||||||||||||||||||||||||||||||||||||***ORDERED POSTS***||||||||||||||||||||||||||||||||||||||||||||||||||\n");
            postStorage.getAllPosts().values().stream().sorted(Comparator.comparing(Post::getLamportTimeStamp)).forEach(post -> {
//                System.out.println("||||||||||||||||||||||||||||||||||||||||||||||||||***POST***||||||||||||||||||||||||||||||||||||||||||||||||||");
                displayPostByID(((Post) post).getPostID(), null);
//                System.out.println("POST >>" + Constants.TAB + ((Post) post).getPostID() + " :::: " + ((Post) post).getPostMessage());
//                ((Post) post).getComments().stream().sorted(Comparator.comparing(Comment::getLamportTimeStamp)).forEach(comment -> System.out.println("COMMENT >>" + Constants.TAB + comment.getCommentID() + " :::: " + comment.getComment()));
//                System.out.println("||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||");
//                System.out.println();
            });
        } else {
            System.out.println("No Posts to display");
        }
    }

    public void displayPostByID(String postID, Post post) {
        if (null != postID) {
            post = postStorage.getPostByID(postID);
        }
        System.out.println("||||||||||||||||||||||||||||||||||||||||||||||||||***POST***||||||||||||||||||||||||||||||||||||||||||||||||||");
        DecimalFormat decimalFormat = new DecimalFormat();
        decimalFormat.setMaximumFractionDigits(2);
        System.out.println("POST >>" + Constants.TAB +"[Rank: "+ decimalFormat.format(post.getAveragePostRank()) + "]" + Constants.SPACE + post.getPostID() + " :::: " + post.getPostMessage());
        post.getComments().forEach(comment -> System.out.println("COMMENT >>" + Constants.TAB + comment.getCommentID() + " :::: " + comment.getComment()));
        System.out.println("||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||");
        System.out.println();
    }
}
